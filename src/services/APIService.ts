import axios, { AxiosResponse } from "axios";
import { Planet } from "../entity/Planet";

export interface APIServiceType {
  getAllPlanetsAPI: () => Promise<Planet[]>;
  addPlanetAPI: (Planet: Planet) => Promise<AxiosResponse<any>>;
  updatePlanetAPI: (id: number, Planet: Planet) => Promise<void>;
  deletePlanetAPI: (id: number) => Promise<void>;
}

export const APIService: APIServiceType = {
  getAllPlanetsAPI: async () => {
    try {
      const response = await axios.get("http://localhost:8000/planets");
      return response.data as Array<Planet>;
    } catch (e) {
      console.error(e);
      return [] as Array<Planet>;
    }
  },

  addPlanetAPI: async (Planet: Planet) => {
    try {
      console.log(Planet);
      const response = await axios.post(
        `http://localhost:8000/planets`,
        Planet
      );
      return response;
    } catch (e) {
      console.error(e);
      return {} as AxiosResponse<any>;
    }
  },

  updatePlanetAPI: async (id: number, Planet: Planet) => {
    try {
      await axios.patch(`http://localhost:8000/planets/${id}`, Planet);
    } catch (e) {
      console.error(e);
    }
  },

  deletePlanetAPI: async (id: number) => {
    try {
      await axios.delete(`http://localhost:8000/planets/${id}`);
    } catch (e) {
      console.error(e);
    }
  },
};
