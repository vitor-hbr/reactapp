import { createContext, ReactNode, useState, useEffect } from "react";

import Loading from "../components/Loading";
import { APIServiceType } from "../services/APIService";
import { Planet } from "../entity/Planet";
import animatingObjectType from "../entity/AnimatingObject";

interface PlanetContextData {
  planets: Planet[];
  showingIndex: number;
  animatingObject: animatingObjectType;
  addPlanet: (newPlanet: Planet) => void;
  editShowingPlanet: (editedPlanet: Planet) => void;
  calculateNewShowingPlanetIndex: (toTheLeft: boolean) => number;
  deletePlanetShowingPlanet: () => void;
  changeShowingPlanet: (toTheLeft: boolean) => void;
  resetAnimationObject: () => void;
  updateShowingIndex: (newIndex: number) => void;
}

export const PlanetContext = createContext({} as PlanetContextData);

interface PlanetProviderProps {
  children: ReactNode;
  APIService: APIServiceType;
}

export function PlanetProvider({ children, APIService }: PlanetProviderProps) {
  const { addPlanetAPI, deletePlanetAPI, updatePlanetAPI, getAllPlanetsAPI } =
    APIService;
  const [planets, setPlanets] = useState<Planet[]>([]);
  const [showingIndex, setShowingIndex] = useState<number>(0);
  const [animatingObject, setAnimatingObject] = useState<animatingObjectType>({
    nextIndex: -1,
    animating: false,
    animatingToTheLeft: false,
  });
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    async function fetchFromAPI() {
      const responseData = await getAllPlanetsAPI();
      responseData.forEach((planet) => {
        setPlanets((oldPlanets) => [...oldPlanets, planet]);
      });
      setIsLoading(false);
    }
    fetchFromAPI();
  }, []);

  const calculateNewShowingPlanetIndex = (toTheLeft: boolean) => {
    let newPlanetIndex = showingIndex;
    if (!toTheLeft) {
      newPlanetIndex++;
      if (newPlanetIndex > planets.length - 1) {
        return 0;
      } else {
        return newPlanetIndex;
      }
    } else {
      newPlanetIndex--;
      if (newPlanetIndex < 0) {
        return planets.length - 1;
      } else {
        return newPlanetIndex;
      }
    }
  };

  const changeShowingPlanet = (toTheLeft: boolean) => {
    if (planets.length > 1) {
      const nextshowingIndex = calculateNewShowingPlanetIndex(toTheLeft);
      setAnimatingObject({
        nextIndex: nextshowingIndex,
        animating: true,
        animatingToTheLeft: toTheLeft,
      });
    }
  };

  const addPlanet = (newPlanet: Planet) => {
    newPlanet.id = planets.length ? planets[planets.length - 1].id + 1 : 1;
    setPlanets((oldArray) => [...oldArray, newPlanet]);
    addPlanetAPI(newPlanet);
  };

  const editShowingPlanet = (editPlanet: Planet) => {
    let newPlanets = [...planets];
    newPlanets[showingIndex].color = editPlanet.color;
    newPlanets[showingIndex].diameter = editPlanet.diameter;
    newPlanets[showingIndex].name = editPlanet.name;
    newPlanets[showingIndex].rotating_speed = editPlanet.rotating_speed;
    setPlanets(newPlanets);
    updatePlanetAPI(editPlanet.id, editPlanet);
  };

  const deletePlanetShowingPlanet = () => {
    if (planets.length > 0) {
      const id = planets[showingIndex].id;
      setPlanets(planets.filter((planet) => planet.id !== id));
      deletePlanetAPI(id);
    }
  };

  const resetAnimationObject = () => {
    setAnimatingObject({
      nextIndex: -1,
      animating: false,
      animatingToTheLeft: false,
    });
  };

  const updateShowingIndex = (newIndex: number) => {
    setShowingIndex(newIndex);
  };

  return (
    <PlanetContext.Provider
      value={{
        planets,
        showingIndex,
        animatingObject,
        addPlanet,
        calculateNewShowingPlanetIndex,
        editShowingPlanet,
        deletePlanetShowingPlanet,
        changeShowingPlanet,
        resetAnimationObject,
        updateShowingIndex,
      }}
    >
      {isLoading && <Loading />}
      {children}
    </PlanetContext.Provider>
  );
}
