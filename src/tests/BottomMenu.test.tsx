import {
  render,
  fireEvent,
  cleanup,
  screen,
  waitForElementToBeRemoved,
} from "./test_utils";

import BottomMenu from "../components/BottomMenu";
import { waitFor } from "@testing-library/react";

afterEach(cleanup);

describe("BottomMenu", () => {
  const openEditModal = jest.fn(() => console.log("chamou"));
  const openAddModal = jest.fn(() => console.log("chamouadd"));

  it("renders snapshot normaly", async () => {
    const { asFragment } = render(
      <BottomMenu EditModalOpen={openEditModal} AddModalOpen={openAddModal} />
    );
    await waitForElementToBeRemoved(screen.getByText("Loading..."));
    expect(asFragment()).toMatchSnapshot();
  });

  it("when clicks button-edit calls EditModal Function", async () => {
    openEditModal.mockReset();
    const { getByTestId } = render(
      <BottomMenu EditModalOpen={openEditModal} AddModalOpen={openAddModal} />
    );
    await waitForElementToBeRemoved(screen.getByText("Loading..."));
    fireEvent.click(getByTestId("button-edit"));
    await waitFor(() => expect(openEditModal).toHaveBeenCalledTimes(1));
  });

  it("when clicks button-add calls AddModal Function", async () => {
    openAddModal.mockReset();
    const { getByTestId } = render(
      <BottomMenu EditModalOpen={openEditModal} AddModalOpen={openAddModal} />
    );
    await waitForElementToBeRemoved(screen.getByText("Loading..."));
    fireEvent.click(getByTestId("button-add"));
    await waitFor(() => expect(openAddModal).toHaveBeenCalledTimes(1));
  });
});
