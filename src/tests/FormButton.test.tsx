import {
  render,
  cleanup,
  screen,
  waitForElementToBeRemoved,
} from "./test_utils";
import { AiOutlineCheck } from "react-icons/ai";

import FormButton from "../components/FormButton";

afterEach(cleanup);

describe("FormButton", () => {
  it("renders snapshot normaly", async () => {
    const { asFragment } = render(
      <FormButton icon={<AiOutlineCheck />} type="submit" size={2.5} />
    );
    await waitForElementToBeRemoved(screen.getByText("Loading..."));
    expect(asFragment()).toMatchSnapshot();
  });
});
