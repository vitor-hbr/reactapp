import React, { FC, ReactElement } from "react";
import { render, RenderOptions } from "@testing-library/react";
import { PlanetProvider } from "../contexts/Planet.context";
import { APIServiceType } from "../services/APIService";

const APIService: APIServiceType = {
  getAllPlanetsAPI: async () => {
    return [
      {
        id: 1,
        name: "Kamino",
        color: "#4282b3",
        diameter: 6,
        rotating_speed: 3.5,
      },
      {
        id: 2,
        name: "Tatooine",
        color: "#ff663e",
        diameter: 11,
        rotating_speed: 22,
      },
      {
        id: 3,
        name: "Dagobah",
        color: "#4b663e",
        diameter: 1,
        rotating_speed: 2,
      },
    ];
  },
  addPlanetAPI: jest.fn(),
  deletePlanetAPI: jest.fn(),
  updatePlanetAPI: jest.fn(),
};

const WithProviders: FC = ({ children }) => {
  return <PlanetProvider APIService={APIService}>{children}</PlanetProvider>;
};

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, "queries">
) => render(ui, { wrapper: WithProviders, ...options });

export * from "@testing-library/react";

export { customRender as render };
