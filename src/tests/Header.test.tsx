import {
  render,
  cleanup,
  screen,
  waitForElementToBeRemoved,
} from "./test_utils";

import Header from "../components/Header";

afterEach(cleanup);

describe("Header", () => {
  it("renders snapshot normaly", async () => {
    const { asFragment } = render(<Header />);
    await waitForElementToBeRemoved(screen.getByText("Loading..."));
    expect(asFragment()).toMatchSnapshot();
  });
});
