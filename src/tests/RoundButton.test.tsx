import {
  render,
  cleanup,
  screen,
  waitForElementToBeRemoved,
} from "./test_utils";
import { AiFillCaretRight } from "react-icons/ai";

import RoundButton from "../components/RoundButton";

afterEach(cleanup);

describe("RoundButton", () => {
  it("renders snapshot normaly", async () => {
    const onClick = jest.fn();

    const { asFragment } = render(
      <RoundButton icon={<AiFillCaretRight />} onClick={onClick} />
    );
    await waitForElementToBeRemoved(screen.getByText("Loading..."));

    expect(asFragment()).toMatchSnapshot();
  });
});
