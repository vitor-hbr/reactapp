import {
  render,
  cleanup,
  screen,
  waitForElementToBeRemoved,
} from "./test_utils";

import Input from "../components/Input";

afterEach(cleanup);

describe("Input", () => {
  it("renders snapshot normaly", async () => {
    const color = "";
    const setColor = (s: string) => {};

    const { asFragment } = render(
      <Input
        type="color"
        placeholder="Color"
        onChange={(e) => setColor(e.target.value)}
        value={color}
      />
    );
    await waitForElementToBeRemoved(screen.getByText("Loading..."));

    expect(asFragment()).toMatchSnapshot();
  });
});
