import {
  render,
  cleanup,
  screen,
  waitForElementToBeRemoved,
} from "./test_utils";

import Modal from "../components/Modal";

afterEach(cleanup);

describe("Modal", () => {
  it("renders snapshot normaly", async () => {
    const addModal = true;
    const closeModal = () => {};

    const { asFragment } = render(
      <Modal addModal={addModal} closeModal={closeModal} />
    );
    await waitForElementToBeRemoved(screen.getByText("Loading..."));

    expect(asFragment()).toMatchSnapshot();
  });
});
