import {
  render,
  cleanup,
  waitForElementToBeRemoved,
  screen,
} from "./test_utils";

import Form from "../components/Form";

afterEach(cleanup);

describe("Form", () => {
  it("renders snapshot normaly", async () => {
    const addModal = true;
    const closeModal = jest.fn();

    const { asFragment } = render(
      <Form addModal={addModal} closeModal={closeModal} />
    );
    await waitForElementToBeRemoved(screen.getByText("Loading..."));

    expect(asFragment()).toMatchSnapshot();
  });
});
