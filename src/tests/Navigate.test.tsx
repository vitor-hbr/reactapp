import {
  render,
  cleanup,
  screen,
  waitForElementToBeRemoved,
} from "./test_utils";

import Navigate from "../components/Navigate";

afterEach(cleanup);

describe("Navigate", () => {
  it("renders snapshot normaly", async () => {
    const { asFragment } = render(<Navigate />);
    await waitForElementToBeRemoved(screen.getByText("Loading..."));
    expect(asFragment()).toMatchSnapshot();
  });
});
