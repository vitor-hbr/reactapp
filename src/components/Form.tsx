import { AiOutlineCheck, AiOutlineClose } from "react-icons/ai";
import { useState, useEffect, useContext, FormEvent } from "react";

import { Planet } from "../entity/Planet";
import FormButton from "./FormButton";
import Input from "./Input";
import { PlanetContext } from "../contexts/Planet.context";

interface FormProps {
  addModal: boolean;
  closeModal: () => void;
}

export default function Form({ addModal, closeModal }: FormProps) {
  const {
    showingIndex,
    planets,
    addPlanet,
    editShowingPlanet,
    animatingObject,
  } = useContext(PlanetContext);

  const [name, setName] = useState("");
  const [color, setColor] = useState("#ffffff");
  const [diameter, setDiameter] = useState(0);
  const [rotatingSpeed, setRotatingSpeed] = useState(0);
  const [id, setId] = useState(0);

  useEffect(() => {
    if (!addModal && planets[showingIndex]) {
      setId(planets[showingIndex].id);
      setName(planets[showingIndex].name);
      setDiameter(planets[showingIndex].diameter);
      setRotatingSpeed(planets[showingIndex].rotating_speed);
      console.log(planets[showingIndex].color);
      setColor(planets[showingIndex].color);
    }
  }, [addModal, showingIndex, planets]);

  const buildPlanet = () => {
    return {
      id: id,
      name: name,
      color: color,
      diameter: diameter,
      rotating_speed: rotatingSpeed,
    } as Planet;
  };

  const confirmPlanet = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const newPlanet: Planet = buildPlanet();
    if (addModal) {
      addPlanet(newPlanet);
    } else {
      if (!animatingObject.animating) editShowingPlanet(newPlanet);
    }
    closeModal();
  };

  return (
    <form onSubmit={confirmPlanet}>
      <Input
        type="text"
        placeholder="Name"
        onChange={(e) => setName(e.target.value)}
        value={name}
      />
      <Input
        type="number"
        placeholder="Diameter"
        onChange={(e) => setDiameter(Number(e.target.value))}
        value={diameter.toString()}
      />
      <Input
        type="number"
        placeholder="Rotating Speed"
        onChange={(e) => setRotatingSpeed(Number(e.target.value))}
        value={rotatingSpeed.toString()}
      />
      <div className="form-end">
        <Input
          type="color"
          placeholder="Color"
          onChange={(e) => setColor(e.target.value)}
          value={color}
        />
        <div className="form-buttons">
          <FormButton type="submit" icon={<AiOutlineCheck />} size={2.5} />
          <FormButton
            type="reset"
            icon={<AiOutlineClose />}
            size={2.5}
            onClick={closeModal}
          />
        </div>
      </div>
    </form>
  );
}
