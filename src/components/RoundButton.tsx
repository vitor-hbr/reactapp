import "../styles/RoundButton.scss";

interface roundButtonProps {
  icon: JSX.Element;
  color?: string;
  size?: number;
  className?: string;
  onClick: () => void;
  data_testid?: string;
}

export default function RoundButton({
  icon,
  color = "#ffde06",
  size = 2,
  className = "",
  onClick,
  data_testid = "",
}: roundButtonProps) {
  return (
    <button
      className={`round-button ${className}`}
      onClick={onClick}
      style={{
        color: color,
        fontSize: `${size}rem`,
      }}
      data-testid={data_testid}
    >
      {icon}
    </button>
  );
}
