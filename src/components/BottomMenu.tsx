import { AiFillPlusCircle, AiFillDelete, AiFillEdit } from "react-icons/ai";
import { useContext } from "react";

import "../styles/BottomMenu.scss";
import RoundButton from "./RoundButton";
import { PlanetContext } from "../contexts/Planet.context";

interface BottomMenuProps {
  AddModalOpen: () => void;
  EditModalOpen: () => void;
}

export default function BottomMenu({
  AddModalOpen,
  EditModalOpen,
}: BottomMenuProps) {
  const { deletePlanetShowingPlanet, animatingObject, planets } =
    useContext(PlanetContext);

  return (
    <div className="bottom-menu">
      <RoundButton
        icon={<AiFillPlusCircle />}
        size={3}
        onClick={() => AddModalOpen()}
        data_testid="button-add"
      />
      <RoundButton
        icon={<AiFillEdit />}
        size={3}
        onClick={() => (planets.length ? EditModalOpen() : {})}
        data_testid="button-edit"
      />
      <RoundButton
        icon={<AiFillDelete />}
        size={3}
        onClick={() =>
          !animatingObject.animating ? deletePlanetShowingPlanet() : {}
        }
        data_testid="button-delete"
      />
    </div>
  );
}
