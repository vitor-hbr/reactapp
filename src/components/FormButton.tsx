import "../styles/FormButton.scss";

interface formButtonProps {
  icon: JSX.Element;
  size?: number;
  className?: string;
  type?: "button" | "submit" | "reset" | undefined;
  onClick?: () => void;
}

export default function FormButton({
  icon,
  size = 1.5,
  type = undefined,
  className = "",
  onClick = undefined,
}: formButtonProps) {
  return (
    <button
      type={type}
      className={`form-button ${className}`}
      onClick={onClick}
      style={{
        fontSize: `${size}rem`,
      }}
    >
      {icon}
    </button>
  );
}
