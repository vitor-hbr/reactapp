import { AiFillCaretLeft, AiFillCaretRight } from "react-icons/ai";
import { useContext } from "react";

import RoundButton from "./RoundButton";
import "../styles/Navigate.scss";
import { PlanetContext } from "../contexts/Planet.context";

export default function Navigate() {
  const { changeShowingPlanet, animatingObject } = useContext(PlanetContext);

  return (
    <div className="navigate">
      <RoundButton
        icon={<AiFillCaretLeft />}
        size={3}
        onClick={() =>
          !animatingObject.animating ? changeShowingPlanet(true) : {}
        }
      />
      <RoundButton
        icon={<AiFillCaretRight />}
        size={3}
        onClick={() =>
          !animatingObject.animating ? changeShowingPlanet(false) : {}
        }
      />
    </div>
  );
}
