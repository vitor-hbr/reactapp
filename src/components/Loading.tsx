import "../styles/Loading.scss";

export default function Loading() {
  return (
    <div className="loading">
      <span>Loading...</span>
    </div>
  );
}
