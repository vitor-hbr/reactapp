import React from 'react';

import "../styles/Header.scss";

export default function Header() {
  return (
    <header>
      <img src="./img/logo.png" alt="StarWars Planets Logo"/>
    </header>
  );
}