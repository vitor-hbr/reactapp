import { useState } from "react";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";

import "../styles/Modal.scss";
import RoundButton from "./RoundButton";
import Form from "./Form";

interface ModalProps {
  addModal: boolean;
  closeModal: () => void;
}

export default function Modal({ addModal, closeModal }: ModalProps) {
  const [showingPreview, setShowingPreview] = useState(false);

  return (
    <div
      className="modal-fade"
      style={{
        background: showingPreview
          ? "rgba(0, 0, 0, 0.25)"
          : "rgba(0, 0, 0, 0.5)",
      }}
    >
      <RoundButton
        className="preview-button"
        icon={!showingPreview ? <AiOutlineEye /> : <AiOutlineEyeInvisible />}
        size={3}
        onClick={() => setShowingPreview(!showingPreview)}
        color="#1f9aff"
      />
      {!showingPreview && (
        <div className="modal-container">
          <span className="modal-title">
            {addModal ? "Build a new Planet" : "Edit Planet"}
          </span>
          <Form addModal={addModal} closeModal={closeModal} />
        </div>
      )}
    </div>
  );
}
