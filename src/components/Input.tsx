import { ChangeEventHandler } from "react";

interface InputProps {
  type: string;
  placeholder: string;
  onChange: ChangeEventHandler<HTMLInputElement>;
  value: string;
}

export default function Input({
  type,
  placeholder,
  onChange,
  value,
}: InputProps) {
  return (
    <input
      className="form-item"
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      required
    />
  );
}
