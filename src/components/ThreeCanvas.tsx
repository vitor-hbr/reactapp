import { useState, useEffect, useRef, useCallback, useContext } from "react";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import * as THREE from "three";

import { PlanetContext } from "../contexts/Planet.context";
import { Planet } from "../entity/Planet";
import "../styles/Canvas.scss";

export default function ThreeCanvas() {
  const {
    planets,
    showingIndex,
    animatingObject,
    resetAnimationObject,
    updateShowingIndex,
  } = useContext(PlanetContext);

  const [ThreePlanets, setThreePlanets] = useState<THREE.Mesh[]>([]);
  const [renderer, setRenderer] = useState<THREE.Renderer | null>(null);
  const [scene, setScene] = useState<THREE.Scene | null>(null);
  const [camera, setCamera] = useState<THREE.PerspectiveCamera | null>(null);
  const [controls, setControls] = useState<OrbitControls | null>(null);

  const canvasRef = useRef<HTMLCanvasElement>();
  const setCanvasCallback = useCallback((canvas: HTMLCanvasElement | null) => {
    if (canvas) {
      (canvasRef.current as unknown) = canvas;
      setupThreeEnviroment();
    }
  }, []);

  const requestRef = useRef<number>(-1);

  useEffect(() => {
    if (scene) {
      if (ThreePlanets.length === planets.length) {
        planetHasBeenEdited();
      } else if (ThreePlanets.length > planets.length) {
        deletePlanet();
      } else {
        buildPlanet(planets[planets.length - 1]);
      }
    }
  }, [planets]);

  useEffect(() => {
    if (ThreePlanets.length > 0) hideExtraPlanets();
  }, [ThreePlanets, showingIndex]);

  useEffect(() => {
    if (requestRef.current !== -1) {
      cancelAnimationFrame(requestRef.current);
      requestRef.current = requestAnimationFrame(animate);
    }
  }, [ThreePlanets, animatingObject]);

  useEffect(() => {
    if (scene && camera && renderer) {
      setupScene();
      buildLights();
      requestRef.current = requestAnimationFrame(animate);
    }
  }, [scene, camera, renderer]);

  useEffect(() => {
    if (controls) setupControls();
  }, [controls]);

  useEffect(() => {
    if (animatingObject.animating) {
      transitionAnimation(animatingObject.animatingToTheLeft);
    }
  }, [animatingObject]);

  const setupThreeEnviroment = () => {
    if (canvasRef.current) {
      createScene();
    }
  };

  const createScene = () => {
    if (canvasRef) {
      setScene(new THREE.Scene());

      setRenderer(new THREE.WebGLRenderer({ canvas: canvasRef.current }));

      setCamera(
        new THREE.PerspectiveCamera(
          70,
          window.innerWidth / window.innerHeight,
          1,
          500
        )
      );
    }
  };

  const setupScene = () => {
    if (canvasRef.current) {
      camera!.position.z = 30;
      camera!.position.y = 5;
      camera!.zoom = 1;
      const loader = new THREE.CubeTextureLoader();
      const texture = loader.load([
        "./img/right.png",
        "./img/left.png",
        "./img/top.png",
        "./img/bottom.png",
        "./img/front.png",
        "./img/back.png",
      ]);
      scene!.background = texture;
      setControls(new OrbitControls(camera!, canvasRef.current));
    }
  };

  const setupControls = () => {
    controls!.target.set(0, 0, 0);
    controls!.autoRotate = true;
    controls!.autoRotateSpeed = 1;
    controls!.update();
  };

  const buildLights = () => {
    if (!scene) return;
    {
      const pointLight = new THREE.PointLight(0xffffff);
      pointLight.position.set(-35, 82, -100);
      scene.add(pointLight);
    }

    {
      const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
      scene.add(ambientLight);
    }
  };

  const removePlanet = (id: number) => {
    if (ThreePlanets.length > 0 && scene) {
      ThreePlanets[showingIndex].geometry.dispose();
      (ThreePlanets[showingIndex].material as any).dispose();
      scene.remove(ThreePlanets[showingIndex]);
      setThreePlanets(
        ThreePlanets.filter((planet) => planet.userData.id !== id)
      );
      let nextIndex = showingIndex - 1;
      updateShowingIndex(nextIndex < 0 ? 0 : nextIndex);
    }
  };

  const deletePlanet = () => {
    if (ThreePlanets.length > 0) {
      const id = ThreePlanets[showingIndex].userData.id;
      removePlanet(id);
    }
  };

  const transitionAnimation = (toTheLeft: boolean) => {
    if (!scene || !controls || !renderer || !camera) return;
    controls.enabled = false;
    controls.autoRotate = false;
    ThreePlanets[animatingObject.nextIndex].visible = true;
    camera.position.set(
      0,
      ThreePlanets[animatingObject.nextIndex].userData.diameter / 2,
      ThreePlanets[animatingObject.nextIndex].userData.diameter * 4
    );
    camera.lookAt(0, 0, 0);
    let offset = 20 * (toTheLeft ? 1 : -1);

    ThreePlanets[animatingObject.nextIndex].position.x = offset;
  };

  const endAnimation = () => {
    ThreePlanets[showingIndex].visible = false;
    ThreePlanets[showingIndex].position.x = 0;
    ThreePlanets[animatingObject.nextIndex].position.x = 0;
    updateShowingIndex(animatingObject.nextIndex);
    resetAnimationObject();
    if (controls) {
      controls.enabled = true;
      controls.autoRotate = true;
      controls.autoRotateSpeed = 1;
    }
  };

  const hideExtraPlanets = () => {
    if (ThreePlanets.length)
      ThreePlanets.forEach((mesh, index) => {
        if (index !== showingIndex) {
          mesh.visible = false;
        } else {
          mesh.visible = true;
          if (camera)
            camera.position.set(
              0,
              ThreePlanets[index].userData.diameter / 2,
              ThreePlanets[index].userData.diameter * 4
            );
        }
      });
  };

  const buildPlanetTitle = (newPlanet: Planet, planetMesh: THREE.Mesh) => {
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");

    canvas.width = 1000;
    canvas.height = 1000;
    if (ctx) {
      ctx.fillStyle = "white";
      ctx.font = `${15 * newPlanet.diameter}px sans-serif`;
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      ctx.fillText(newPlanet.name, 500, 500);

      const textMesh = new THREE.Mesh(
        new THREE.PlaneBufferGeometry(13, 13),
        new THREE.MeshBasicMaterial({
          transparent: true,
          map: new THREE.CanvasTexture(canvas),
        })
      );
      textMesh.position.y += newPlanet.diameter * 1.2;
      planetMesh.add(textMesh);
    }
  };

  const buildPlanet = (newPlanet: Planet) => {
    if (scene) {
      const sphereGeo = new THREE.SphereBufferGeometry(
        newPlanet.diameter,
        30,
        30
      );
      const sphereMat = new THREE.MeshStandardMaterial({
        color: newPlanet.color,
        roughness: 0.6,
        metalness: 0.1,
      });
      const mesh = new THREE.Mesh(sphereGeo, sphereMat);

      mesh.userData.id = newPlanet.id;
      mesh.userData.name = newPlanet.name;
      mesh.userData.rotatingSpeed = newPlanet.rotating_speed / 10;
      mesh.userData.diameter = newPlanet.diameter;
      mesh.userData.color = newPlanet.color;

      buildPlanetTitle(newPlanet, mesh);
      mesh.visible = false;
      scene.add(mesh);
      setThreePlanets((oldArray) => [...oldArray, mesh]);
    }
  };

  const resizeRendererToDisplaySize = (renderer: THREE.Renderer) => {
    const canvas = renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.width !== width || canvas.height !== height;
    if (needResize) {
      renderer.setSize(width, height, false);
    }
    return needResize;
  };

  const animate = (time: number) => {
    time *= 0.001;
    if (camera && scene && renderer) {
      ThreePlanets.forEach((planet) => {
        planet.rotateY((planet.userData.rotatingSpeed * time) / (60 * 24));
        if (planet.children[0]) planet.children[0].lookAt(camera.position);
      });

      let offset = 20;
      let frames = 60;

      if (
        animatingObject.animating &&
        animatingObject.nextIndex !== -1 &&
        controls?.enabled === false
      ) {
        if (animatingObject.animatingToTheLeft) {
          if (ThreePlanets[animatingObject.nextIndex].position.x >= 0) {
            ThreePlanets[animatingObject.nextIndex].position.x -=
              offset / frames;
            ThreePlanets[showingIndex].position.x -= offset / frames;
          } else {
            if (ThreePlanets[showingIndex].visible) {
              endAnimation();
              return;
            }
          }
        } else {
          if (ThreePlanets[animatingObject.nextIndex].position.x <= 0) {
            ThreePlanets[animatingObject.nextIndex].position.x +=
              offset / frames;
            ThreePlanets[showingIndex].position.x += offset / frames;
          } else {
            if (ThreePlanets[showingIndex].visible) {
              endAnimation();
              return;
            }
          }
        }
      }

      if (resizeRendererToDisplaySize(renderer!)) {
        const canvas = renderer!.domElement;
        camera.aspect = canvas.clientWidth / canvas.clientHeight;
        camera.updateProjectionMatrix();
      }
      renderer.render(scene, camera);
      if (controls) controls.update();
    }

    requestAnimationFrame(animate);
  };

  const planetHasBeenEdited = () => {
    if (!scene) return;
    let newThreePlanets = [...ThreePlanets];
    newThreePlanets.forEach((planet, index) => {
      if (
        planet.userData.name !== planets[index].name ||
        planet.userData.diameter !== planets[index].diameter
      ) {
        (planet.children[0] as THREE.Mesh).geometry.dispose();
        (
          (planet.children[0] as THREE.Mesh).material as THREE.Material
        ).dispose();
        scene.remove(planet.children[0] as THREE.Mesh);
        planet.children.splice(0, 1);
        buildPlanetTitle(planets[index], planet);
      }

      if (planet.userData.diameter !== planets[index].diameter) {
        planet.geometry.dispose();
        const sphereGeo = new THREE.SphereBufferGeometry(
          planets[index].diameter,
          30,
          30
        );
        planet.geometry.copy(sphereGeo);
        planet.userData.diameter = planets[index].diameter;
      }
      if (
        planet.userData.rotatingSpeed !==
        planets[index].rotating_speed / 10
      ) {
        planet.userData.rotatingSpeed = planets[index].rotating_speed;
      }
      if (planet.userData.color !== planets[index].color) {
        planet.userData.color = planets[index].color;
        (planet.material as THREE.MeshStandardMaterial).color.setHex(
          parseInt(planets[index].color.replace(/^#/, ""), 16)
        );
      }
    });
    setThreePlanets(newThreePlanets);
  };

  return <canvas ref={setCanvasCallback} id="canvas"></canvas>;
}
