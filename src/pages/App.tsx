import { useState } from "react";
//import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";

import "../styles/App.scss";

import Header from "../components/Header";
import Navigate from "../components/Navigate";
import BottomMenu from "../components/BottomMenu";
import Modal from "../components/Modal";
import ThreeCanvas from "../components/ThreeCanvas";

import { APIService } from "../services/APIService";
import { PlanetProvider } from "../contexts/Planet.context";

function App() {
  const [isAddModalOpen, setIsAddModalOpen] = useState<boolean>(false);
  const [isEditModalOpen, setIsEditModalOpen] = useState<boolean>(false);

  return (
    <>
      <Header />
      <PlanetProvider APIService={APIService}>
        <Navigate />
        <ThreeCanvas />
        <BottomMenu
          AddModalOpen={() => setIsAddModalOpen(true)}
          EditModalOpen={() => setIsEditModalOpen(true)}
        />
        {(isAddModalOpen || isEditModalOpen) && (
          <Modal
            addModal={isAddModalOpen}
            closeModal={() => {
              setIsAddModalOpen(false);
              setIsEditModalOpen(false);
            }}
          />
        )}
      </PlanetProvider>
    </>
  );
}

export default App;
