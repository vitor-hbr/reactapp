export default interface animatingObjectType {
  nextIndex: number;
  animating: boolean;
  animatingToTheLeft: boolean;
}
