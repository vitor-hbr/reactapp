export interface Planet {
  id: number;
  name: string;
  color: string;
  diameter: number;
  rotating_speed: number;
}
