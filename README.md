# Project Star Wars Planets CRUD 
### Application made as the assignmnet of the P2P UI Essentials React Module.

## 🗓 Project Description 
##### In this application you can look around and watch Star Wars Planets rotating, add new Planets, and edit or delete existing ones. 
## 🏁 To use the application
```
# install dependencies
$ yarn

# start JSON Server
$ yarn server

# serve with hot reload at localhost:3000
$ yarn start
```

## 📊 Test
##### Unit test using Jest.
```
$ yarn test
```

## 💻 Technologies 
* ReactJS
* ThreeJS
* JSON Server
* Sass
* Unit Test using Jest and React Testing Library
